# INSTALL DJANGO
python3 -m pip install Django

# CREATE FIRST PROJECT
django-admin startproject vidly .

# CHECK VERSION
python3 -m django --version

# WSGI stands for Web Server Gateway Interface

# RUN SERVER
python3 manage.py runserver

# START APPLICATION
python3 manage.py startapp movies

# MIGRATE DB
python3 manage.py makemigrations
python3 manage.py migrate

# CREATE SUPERUSER
python3 manage.py createsuperuser
username: abhisek
email: abhisek.dutta.507@gmail.com
password: Django@12345

# CREATE API
pipenv install django-tastypie
python3 -m pip install django-tastypie

# COLLECT STATIC
python3 manage.py collectstatic

# SERVE STATIC FILES ON HEROKU
pipenv install whitenoise
python3 -m pip install whitenoise
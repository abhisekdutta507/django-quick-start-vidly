from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from .models import Movie

# Create your views here.


def index(req):
    movies = Movie.objects.all()
    output = ', '.join([m.title for m in movies])
    # return HttpResponse(output)

    # return HTML template with workspace
    return render(req, 'movies/index.html', {'movies': movies})


def detail(req, movie_id):
    # way 1 to render the template
    # try:
    #     movie = Movie.objects.get(pk=movie_id)
    #     return render(req, 'movies/detail.html', {'movie': movie})
    # except Movie.DoesNotExist:
    #     raise Http404()

    # way 2 to render template
    movie = get_object_or_404(Movie, pk=movie_id)
    return render(req, 'movies/detail.html', {'movie': movie})

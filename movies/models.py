from django.db import models
from django.utils import timezone
# Connect MongoDB using Djongo here https://nesdis.github.io/djongo/get-started/
# Create your models here.
# Inheritance


class Genre(models.Model):
    # keyword arguments
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField(max_length=255)
    release_year = models.IntegerField()
    number_in_stock = models.IntegerField()
    daily_rate = models.FloatField()
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title
